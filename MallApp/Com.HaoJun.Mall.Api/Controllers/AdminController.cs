using Com.HaoJun.Mall.Api.Models;
using Com.HaoJun.Mall.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace Com.HaoJun.Mall.Api.Controllers
{
    public class AdminController : ApiController
    {
        Com.HaoJun.Mall.BLL.AdminUser bll = new BLL.AdminUser();
        [HttpGet]
        public ApiReturnObject<int> Login(string name, string pwd)
        {
            ApiReturnObject<int> obj = new ApiReturnObject<int>();
            var list = bll.GetModelList("UserName='" + name + "'and PassWord='" + pwd + "'");
            if (list != null && list.Count > 0)
            {
                obj.result = 0;
                obj.returncode = 0;
                obj.message = "登陆成功";
            }
            return obj;
        }

        [HttpGet]
        public ApiReturnObject<int> Delete(int id)
        {
            ApiReturnObject<int> obj = new ApiReturnObject<int>();
            if (id > 0)
            {
                bll.Delete(id);
                obj.result = 0;
                obj.returncode = 0;
                obj.message = "删除成功";
            }
            return obj;
        }
        [HttpPost]
        public ApiReturnObject<int> Edit(Model.AdminUser model)
        {
            ApiReturnObject<int> obj = new ApiReturnObject<int>();
            if (model.Id > 0)
            {
                bll.Update(model);
                obj.result = 0;
                obj.returncode = 0;
                obj.message = "修改成功";
            }
            else
            {
                bll.Add(model);
                obj.result = 0;
                obj.returncode = 0;
                obj.message = "添加成功";
            }
            return obj;
        }
        public ApiReturnObject<List<AdminUser>> GetList()
        {
            ApiReturnObject<List<AdminUser>> obj = new ApiReturnObject<List<AdminUser>>();
            List<AdminUser> adminUsers =  bll.GetModelList("");
            obj.result = adminUsers;
                return obj;
        }
}
}


