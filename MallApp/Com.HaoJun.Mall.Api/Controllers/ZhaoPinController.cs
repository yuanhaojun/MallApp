﻿using Com.HaoJun.Mall.Api.Models;
using Com.HaoJun.Mall.Api.Models.Respose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Com.HaoJun.Mall.Api.Controllers
{
    public class ZhaoPinController : ApiController
    {
        [HttpGet]
        public ApiReturnObject<ZhaoPinDemo> ZhaoPin()
        {
            ApiReturnObject<ZhaoPinDemo> obj = new ApiReturnObject<ZhaoPinDemo>();
            ZhaoPinDemo zp = new ZhaoPinDemo
            {
                description = "为求职者提供真实准确的北京求职信息及公司简介，帮助求职者更好更快更全面地了解行业招聘信息。助您",
                keyWords = "求职信息，最新招聘",
                title = "信息_求职信息_找工作上智"
            };
            obj.result = zp;
            
            return obj;
         }
    }
}
