﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.HaoJun.Mall.Api.Models
{
    public class ApiReturnObject<T>
    {
        public int returncode { get; set; }
        public string message { get; set; }
        public T result { get; set; }  
    }
}