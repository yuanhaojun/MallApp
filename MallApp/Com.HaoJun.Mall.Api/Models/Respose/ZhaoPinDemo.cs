﻿namespace Com.HaoJun.Mall.Api.Models.Respose
{
    public class ZhaoPinDemo
    {
        /// <summary>
        /// 描述
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 关键词
        /// </summary>
        public string keyWords { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }
    }
}