﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.HaoJun.Mall.Api.Models.Request
{
    public class DemoRequest
    {
        public int id { get; set; }
        public string name { get; set; }
        public string adress { get; set; }
    }
}