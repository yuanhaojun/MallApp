﻿/**  版本信息模板在安装目录下，可自行修改。
* GoodsPropertyValRelation.cs
*
* 功 能： N/A
* 类 名： GoodsPropertyValRelation
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:28   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Com.HaoJun.Mall.Model
{
	/// <summary>
	/// 商品属性关系表
	/// </summary>
	[Serializable]
	public partial class GoodsPropertyValRelation
	{
		public GoodsPropertyValRelation()
		{}
		#region Model
		private int _id;
		private int _goodsid=0;
		private string _valueids="";
		private int _goodsnum=0;
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 商品ID
		/// </summary>
		public int GoodsId
		{
			set{ _goodsid=value;}
			get{return _goodsid;}
		}
		/// <summary>
		/// 属性值ID（多个ID）
		/// </summary>
		public string ValueIds
		{
			set{ _valueids=value;}
			get{return _valueids;}
		}
		/// <summary>
		/// 商品数量
		/// </summary>
		public int GoodsNum
		{
			set{ _goodsnum=value;}
			get{return _goodsnum;}
		}
		#endregion Model

	}
}

