﻿/**  版本信息模板在安装目录下，可自行修改。
* ShopCar.cs
*
* 功 能： N/A
* 类 名： ShopCar
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:29   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Com.HaoJun.Mall.Model
{
	/// <summary>
	/// 购物车
	/// </summary>
	[Serializable]
	public partial class ShopCar
	{
		public ShopCar()
		{}
		#region Model
		private int _id;
		private int _useid=0;
		private int _goodsid=0;
		private string _goodsname="";
		private decimal _goodsprice=0M;
		private int _goodsnum=0;
		private DateTime _creatdate= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 用户ID
		/// </summary>
		public int UseId
		{
			set{ _useid=value;}
			get{return _useid;}
		}
		/// <summary>
		/// 商品ID
		/// </summary>
		public int GoodsId
		{
			set{ _goodsid=value;}
			get{return _goodsid;}
		}
		/// <summary>
		/// 商品名
		/// </summary>
		public string GoodsName
		{
			set{ _goodsname=value;}
			get{return _goodsname;}
		}
		/// <summary>
		/// 商品价格
		/// </summary>
		public decimal GoodsPrice
		{
			set{ _goodsprice=value;}
			get{return _goodsprice;}
		}
		/// <summary>
		/// 商品数量
		/// </summary>
		public int GoodsNum
		{
			set{ _goodsnum=value;}
			get{return _goodsnum;}
		}
		/// <summary>
		/// 创建日期
		/// </summary>
		public DateTime CreatDate
		{
			set{ _creatdate=value;}
			get{return _creatdate;}
		}
		#endregion Model

	}
}

