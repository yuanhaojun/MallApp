﻿/**  版本信息模板在安装目录下，可自行修改。
* GoodsInfo.cs
*
* 功 能： N/A
* 类 名： GoodsInfo
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:27   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Com.HaoJun.Mall.Model
{
	/// <summary>
	/// 商品总表
	/// </summary>
	[Serializable]
	public partial class GoodsInfo
	{
		public GoodsInfo()
		{}
		#region Model
		private int _id;
		private int _goodscateid=0;
		private string _goodssea="";
		private string _goodsname="";
		private string _goodstitle="";
		private decimal _goodsprice=0M;
		private string _goodsems="";
		private int _goodsnum=0;
		private int _goodssale=0;
		private string _goodsprovname="";
		private string _goodsctiyname="";
		private string _goodspicurl="";
		private string _goodstag="";
		private string _goodsdetail="";
		private int _state=0;
		private int _ishot=2;
		private DateTime _createdate= DateTime.Now;
		/// <summary>
		/// 商品自增长ID
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 商品类别ID
		/// </summary>
		public int GoodsCateId
		{
			set{ _goodscateid=value;}
			get{return _goodscateid;}
		}
		/// <summary>
		/// 商品季节
		/// </summary>
		public string GoodsSea
		{
			set{ _goodssea=value;}
			get{return _goodssea;}
		}
		/// <summary>
		/// 商品名
		/// </summary>
		public string GoodsName
		{
			set{ _goodsname=value;}
			get{return _goodsname;}
		}
		/// <summary>
		/// 副标题
		/// </summary>
		public string GoodsTitle
		{
			set{ _goodstitle=value;}
			get{return _goodstitle;}
		}
		/// <summary>
		/// 商品价格
		/// </summary>
		public decimal GoodsPrice
		{
			set{ _goodsprice=value;}
			get{return _goodsprice;}
		}
		/// <summary>
		/// 快递方式
		/// </summary>
		public string GoodsEms
		{
			set{ _goodsems=value;}
			get{return _goodsems;}
		}
		/// <summary>
		/// 库存
		/// </summary>
		public int GoodsNum
		{
			set{ _goodsnum=value;}
			get{return _goodsnum;}
		}
		/// <summary>
		/// 销量
		/// </summary>
		public int GoodsSale
		{
			set{ _goodssale=value;}
			get{return _goodssale;}
		}
		/// <summary>
		/// 商品所在省份
		/// </summary>
		public string GoodsProvName
		{
			set{ _goodsprovname=value;}
			get{return _goodsprovname;}
		}
		/// <summary>
		/// 商品所在城市
		/// </summary>
		public string GoodsCtiyName
		{
			set{ _goodsctiyname=value;}
			get{return _goodsctiyname;}
		}
		/// <summary>
		/// 商品图片地址（用逗号隔开）
		/// </summary>
		public string GoodsPicUrl
		{
			set{ _goodspicurl=value;}
			get{return _goodspicurl;}
		}
		/// <summary>
		/// 商品标签（正品保证,7天退换,保修）
		/// </summary>
		public string GoodsTag
		{
			set{ _goodstag=value;}
			get{return _goodstag;}
		}
		/// <summary>
		/// 商品详情
		/// </summary>
		public string GoodsDetail
		{
			set{ _goodsdetail=value;}
			get{return _goodsdetail;}
		}
		/// <summary>
		/// 0上架1下架
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 1热销2新品
		/// </summary>
		public int IsHot
		{
			set{ _ishot=value;}
			get{return _ishot;}
		}
		/// <summary>
		/// 商品创建时间
		/// </summary>
		public DateTime CreateDate
		{
			set{ _createdate=value;}
			get{return _createdate;}
		}
		#endregion Model

	}
}

