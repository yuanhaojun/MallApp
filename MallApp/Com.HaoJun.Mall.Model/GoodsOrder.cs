﻿/**  版本信息模板在安装目录下，可自行修改。
* GoodsOrder.cs
*
* 功 能： N/A
* 类 名： GoodsOrder
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:28   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Com.HaoJun.Mall.Model
{
	/// <summary>
	/// 订单表
	/// </summary>
	[Serializable]
	public partial class GoodsOrder
	{
		public GoodsOrder()
		{}
		#region Model
		private int _id;
		private string _ordernum="";
		private int _userid=0;
		private decimal _totalprice=0M;
		private DateTime _paydate= Convert.ToDateTime(1990/1/1);
		private int _state=0;
		private decimal _payprice=0M;
		private int _adressid=0;
		private string _valueids="";
		private DateTime _creatdate= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 订单号
		/// </summary>
		public string OrderNum
		{
			set{ _ordernum=value;}
			get{return _ordernum;}
		}
		/// <summary>
		/// 用户ID
		/// </summary>
		public int UserId
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		/// <summary>
		/// 订单总价
		/// </summary>
		public decimal TotalPrice
		{
			set{ _totalprice=value;}
			get{return _totalprice;}
		}
		/// <summary>
		/// 支付日期
		/// </summary>
		public DateTime PayDate
		{
			set{ _paydate=value;}
			get{return _paydate;}
		}
		/// <summary>
		/// 0未支付，1已支付，2发货，3已完成，4已关闭，5已退款
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 支付金额
		/// </summary>
		public decimal PayPrice
		{
			set{ _payprice=value;}
			get{return _payprice;}
		}
		/// <summary>
		/// 收货地址ID
		/// </summary>
		public int AdressId
		{
			set{ _adressid=value;}
			get{return _adressid;}
		}
		/// <summary>
		/// 商品属性（多个ID组成）
		/// </summary>
		public string ValueIds
		{
			set{ _valueids=value;}
			get{return _valueids;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime CreatDate
		{
			set{ _creatdate=value;}
			get{return _creatdate;}
		}
		#endregion Model

	}
}

