﻿/**  版本信息模板在安装目录下，可自行修改。
* PropertyValues.cs
*
* 功 能： N/A
* 类 名： PropertyValues
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:28   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Com.HaoJun.Mall.Model
{
	/// <summary>
	/// 商品属性值表
	/// </summary>
	[Serializable]
	public partial class PropertyValues
	{
		public PropertyValues()
		{}
		#region Model
		private int _id;
		private int _proid=0;
		private string _provalue="";
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 商品属性ID
		/// </summary>
		public int ProId
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 商品属性值
		/// </summary>
		public string ProValue
		{
			set{ _provalue=value;}
			get{return _provalue;}
		}
		#endregion Model

	}
}

