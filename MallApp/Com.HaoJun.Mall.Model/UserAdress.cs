﻿/**  版本信息模板在安装目录下，可自行修改。
* UserAdress.cs
*
* 功 能： N/A
* 类 名： UserAdress
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:29   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Com.HaoJun.Mall.Model
{
	/// <summary>
	/// 收货地址表
	/// </summary>
	[Serializable]
	public partial class UserAdress
	{
		public UserAdress()
		{}
		#region Model
		private int _id;
		private int _userid=0;
		private string _username="";
		private string _phone="";
		private string _adress="";
		private string _zipcode="";
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 用户ID
		/// </summary>
		public int UserId
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		/// <summary>
		/// 用户名字
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 电话
		/// </summary>
		public string Phone
		{
			set{ _phone=value;}
			get{return _phone;}
		}
		/// <summary>
		/// 收货地址
		/// </summary>
		public string Adress
		{
			set{ _adress=value;}
			get{return _adress;}
		}
		/// <summary>
		/// 邮编号
		/// </summary>
		public string ZipCode
		{
			set{ _zipcode=value;}
			get{return _zipcode;}
		}
		#endregion Model

	}
}

