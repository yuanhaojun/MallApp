﻿/**  版本信息模板在安装目录下，可自行修改。
* AdminUser.cs
*
* 功 能： N/A
* 类 名： AdminUser
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/12/1 0:07:51   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Runtime.Serialization;

namespace Com.HaoJun.Mall.Model
{
    /// <summary>
    /// AdminUser:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary> 
    public partial class AdminUser
    {
        public AdminUser()
        { }
        #region Model
        private int _isadmin;
        /// <summary>
        ///1超级管理员0普通管理员
        /// </summary>
        [DataMember]
        public int IsAdmin
        {
            get { return _isadmin; }
            set { _isadmin = value; }
        }
        private int _id;
        private string _username = "";
        private string _password = "";
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string UserName
        {
            set { _username = value; }
            get { return _username; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string PassWord
        {
            set { _password = value; }
            get { return _password; }
        }
        #endregion Model

    }
}

