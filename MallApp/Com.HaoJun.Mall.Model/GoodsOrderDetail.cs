﻿/**  版本信息模板在安装目录下，可自行修改。
* GoodsOrderDetail.cs
*
* 功 能： N/A
* 类 名： GoodsOrderDetail
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:28   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Com.HaoJun.Mall.Model
{
	/// <summary>
	/// 订单详情表
	/// </summary>
	[Serializable]
	public partial class GoodsOrderDetail
	{
		public GoodsOrderDetail()
		{}
		#region Model
		private int _id;
		private string _ordernum="";
		private int _userid=0;
		private string _goodsname="";
		private decimal _goodsprice=0M;
		private int _goodsnum=0;
		private int _relationid=0;
		private DateTime _creatdate= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 订单号
		/// </summary>
		public string OrderNum
		{
			set{ _ordernum=value;}
			get{return _ordernum;}
		}
		/// <summary>
		/// 用户ID
		/// </summary>
		public int UserId
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		/// <summary>
		/// 商品名
		/// </summary>
		public string GoodsName
		{
			set{ _goodsname=value;}
			get{return _goodsname;}
		}
		/// <summary>
		/// 商品价格
		/// </summary>
		public decimal GoodsPrice
		{
			set{ _goodsprice=value;}
			get{return _goodsprice;}
		}
		/// <summary>
		/// 商品数量
		/// </summary>
		public int GoodsNum
		{
			set{ _goodsnum=value;}
			get{return _goodsnum;}
		}
		/// <summary>
		/// 商品属性ID
		/// </summary>
		public int RelationId
		{
			set{ _relationid=value;}
			get{return _relationid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime CreatDate
		{
			set{ _creatdate=value;}
			get{return _creatdate;}
		}
		#endregion Model

	}
}

