﻿/**  版本信息模板在安装目录下，可自行修改。
* GoodsInfo.cs
*
* 功 能： N/A
* 类 名： GoodsInfo
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:27   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Com.HaoJun.Mall.DB;
namespace Com.HaoJun.Mall.DAL
{
	/// <summary>
	/// 数据访问类:GoodsInfo
	/// </summary>
	public partial class GoodsInfo
	{
		public GoodsInfo()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "GoodsInfo"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from GoodsInfo");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Com.HaoJun.Mall.Model.GoodsInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into GoodsInfo(");
			strSql.Append("GoodsCateId,GoodsSea,GoodsName,GoodsTitle,GoodsPrice,GoodsEms,GoodsNum,GoodsSale,GoodsProvName,GoodsCtiyName,GoodsPicUrl,GoodsTag,GoodsDetail,State,IsHot,CreateDate)");
			strSql.Append(" values (");
			strSql.Append("@GoodsCateId,@GoodsSea,@GoodsName,@GoodsTitle,@GoodsPrice,@GoodsEms,@GoodsNum,@GoodsSale,@GoodsProvName,@GoodsCtiyName,@GoodsPicUrl,@GoodsTag,@GoodsDetail,@State,@IsHot,@CreateDate)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@GoodsCateId", SqlDbType.Int,4),
					new SqlParameter("@GoodsSea", SqlDbType.NVarChar,70),
					new SqlParameter("@GoodsName", SqlDbType.NVarChar,200),
					new SqlParameter("@GoodsTitle", SqlDbType.NVarChar,400),
					new SqlParameter("@GoodsPrice", SqlDbType.Decimal,9),
					new SqlParameter("@GoodsEms", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsNum", SqlDbType.Int,4),
					new SqlParameter("@GoodsSale", SqlDbType.Int,4),
					new SqlParameter("@GoodsProvName", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsCtiyName", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsPicUrl", SqlDbType.NVarChar,1000),
					new SqlParameter("@GoodsTag", SqlDbType.NVarChar,100),
					new SqlParameter("@GoodsDetail", SqlDbType.Text),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@IsHot", SqlDbType.Int,4),
					new SqlParameter("@CreateDate", SqlDbType.DateTime)};
			parameters[0].Value = model.GoodsCateId;
			parameters[1].Value = model.GoodsSea;
			parameters[2].Value = model.GoodsName;
			parameters[3].Value = model.GoodsTitle;
			parameters[4].Value = model.GoodsPrice;
			parameters[5].Value = model.GoodsEms;
			parameters[6].Value = model.GoodsNum;
			parameters[7].Value = model.GoodsSale;
			parameters[8].Value = model.GoodsProvName;
			parameters[9].Value = model.GoodsCtiyName;
			parameters[10].Value = model.GoodsPicUrl;
			parameters[11].Value = model.GoodsTag;
			parameters[12].Value = model.GoodsDetail;
			parameters[13].Value = model.State;
			parameters[14].Value = model.IsHot;
			parameters[15].Value = model.CreateDate;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Com.HaoJun.Mall.Model.GoodsInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update GoodsInfo set ");
			strSql.Append("GoodsCateId=@GoodsCateId,");
			strSql.Append("GoodsSea=@GoodsSea,");
			strSql.Append("GoodsName=@GoodsName,");
			strSql.Append("GoodsTitle=@GoodsTitle,");
			strSql.Append("GoodsPrice=@GoodsPrice,");
			strSql.Append("GoodsEms=@GoodsEms,");
			strSql.Append("GoodsNum=@GoodsNum,");
			strSql.Append("GoodsSale=@GoodsSale,");
			strSql.Append("GoodsProvName=@GoodsProvName,");
			strSql.Append("GoodsCtiyName=@GoodsCtiyName,");
			strSql.Append("GoodsPicUrl=@GoodsPicUrl,");
			strSql.Append("GoodsTag=@GoodsTag,");
			strSql.Append("GoodsDetail=@GoodsDetail,");
			strSql.Append("State=@State,");
			strSql.Append("IsHot=@IsHot,");
			strSql.Append("CreateDate=@CreateDate");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@GoodsCateId", SqlDbType.Int,4),
					new SqlParameter("@GoodsSea", SqlDbType.NVarChar,70),
					new SqlParameter("@GoodsName", SqlDbType.NVarChar,200),
					new SqlParameter("@GoodsTitle", SqlDbType.NVarChar,400),
					new SqlParameter("@GoodsPrice", SqlDbType.Decimal,9),
					new SqlParameter("@GoodsEms", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsNum", SqlDbType.Int,4),
					new SqlParameter("@GoodsSale", SqlDbType.Int,4),
					new SqlParameter("@GoodsProvName", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsCtiyName", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsPicUrl", SqlDbType.NVarChar,1000),
					new SqlParameter("@GoodsTag", SqlDbType.NVarChar,100),
					new SqlParameter("@GoodsDetail", SqlDbType.Text),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@IsHot", SqlDbType.Int,4),
					new SqlParameter("@CreateDate", SqlDbType.DateTime),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.GoodsCateId;
			parameters[1].Value = model.GoodsSea;
			parameters[2].Value = model.GoodsName;
			parameters[3].Value = model.GoodsTitle;
			parameters[4].Value = model.GoodsPrice;
			parameters[5].Value = model.GoodsEms;
			parameters[6].Value = model.GoodsNum;
			parameters[7].Value = model.GoodsSale;
			parameters[8].Value = model.GoodsProvName;
			parameters[9].Value = model.GoodsCtiyName;
			parameters[10].Value = model.GoodsPicUrl;
			parameters[11].Value = model.GoodsTag;
			parameters[12].Value = model.GoodsDetail;
			parameters[13].Value = model.State;
			parameters[14].Value = model.IsHot;
			parameters[15].Value = model.CreateDate;
			parameters[16].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from GoodsInfo ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from GoodsInfo ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Com.HaoJun.Mall.Model.GoodsInfo GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,GoodsCateId,GoodsSea,GoodsName,GoodsTitle,GoodsPrice,GoodsEms,GoodsNum,GoodsSale,GoodsProvName,GoodsCtiyName,GoodsPicUrl,GoodsTag,GoodsDetail,State,IsHot,CreateDate from GoodsInfo ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			Com.HaoJun.Mall.Model.GoodsInfo model=new Com.HaoJun.Mall.Model.GoodsInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Com.HaoJun.Mall.Model.GoodsInfo DataRowToModel(DataRow row)
		{
			Com.HaoJun.Mall.Model.GoodsInfo model=new Com.HaoJun.Mall.Model.GoodsInfo();
			if (row != null)
			{
				if(row["Id"]!=null && row["Id"].ToString()!="")
				{
					model.Id=int.Parse(row["Id"].ToString());
				}
				if(row["GoodsCateId"]!=null && row["GoodsCateId"].ToString()!="")
				{
					model.GoodsCateId=int.Parse(row["GoodsCateId"].ToString());
				}
				if(row["GoodsSea"]!=null)
				{
					model.GoodsSea=row["GoodsSea"].ToString();
				}
				if(row["GoodsName"]!=null)
				{
					model.GoodsName=row["GoodsName"].ToString();
				}
				if(row["GoodsTitle"]!=null)
				{
					model.GoodsTitle=row["GoodsTitle"].ToString();
				}
				if(row["GoodsPrice"]!=null && row["GoodsPrice"].ToString()!="")
				{
					model.GoodsPrice=decimal.Parse(row["GoodsPrice"].ToString());
				}
				if(row["GoodsEms"]!=null)
				{
					model.GoodsEms=row["GoodsEms"].ToString();
				}
				if(row["GoodsNum"]!=null && row["GoodsNum"].ToString()!="")
				{
					model.GoodsNum=int.Parse(row["GoodsNum"].ToString());
				}
				if(row["GoodsSale"]!=null && row["GoodsSale"].ToString()!="")
				{
					model.GoodsSale=int.Parse(row["GoodsSale"].ToString());
				}
				if(row["GoodsProvName"]!=null)
				{
					model.GoodsProvName=row["GoodsProvName"].ToString();
				}
				if(row["GoodsCtiyName"]!=null)
				{
					model.GoodsCtiyName=row["GoodsCtiyName"].ToString();
				}
				if(row["GoodsPicUrl"]!=null)
				{
					model.GoodsPicUrl=row["GoodsPicUrl"].ToString();
				}
				if(row["GoodsTag"]!=null)
				{
					model.GoodsTag=row["GoodsTag"].ToString();
				}
				if(row["GoodsDetail"]!=null)
				{
					model.GoodsDetail=row["GoodsDetail"].ToString();
				}
				if(row["State"]!=null && row["State"].ToString()!="")
				{
					model.State=int.Parse(row["State"].ToString());
				}
				if(row["IsHot"]!=null && row["IsHot"].ToString()!="")
				{
					model.IsHot=int.Parse(row["IsHot"].ToString());
				}
				if(row["CreateDate"]!=null && row["CreateDate"].ToString()!="")
				{
					model.CreateDate=DateTime.Parse(row["CreateDate"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,GoodsCateId,GoodsSea,GoodsName,GoodsTitle,GoodsPrice,GoodsEms,GoodsNum,GoodsSale,GoodsProvName,GoodsCtiyName,GoodsPicUrl,GoodsTag,GoodsDetail,State,IsHot,CreateDate ");
			strSql.Append(" FROM GoodsInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,GoodsCateId,GoodsSea,GoodsName,GoodsTitle,GoodsPrice,GoodsEms,GoodsNum,GoodsSale,GoodsProvName,GoodsCtiyName,GoodsPicUrl,GoodsTag,GoodsDetail,State,IsHot,CreateDate ");
			strSql.Append(" FROM GoodsInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM GoodsInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from GoodsInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "GoodsInfo";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

