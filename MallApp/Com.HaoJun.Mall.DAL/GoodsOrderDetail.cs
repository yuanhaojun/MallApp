﻿/**  版本信息模板在安装目录下，可自行修改。
* GoodsOrderDetail.cs
*
* 功 能： N/A
* 类 名： GoodsOrderDetail
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:28   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Com.HaoJun.Mall.DB;
namespace Com.HaoJun.Mall.DAL
{
	/// <summary>
	/// 数据访问类:GoodsOrderDetail
	/// </summary>
	public partial class GoodsOrderDetail
	{
		public GoodsOrderDetail()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "GoodsOrderDetail"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from GoodsOrderDetail");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Com.HaoJun.Mall.Model.GoodsOrderDetail model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into GoodsOrderDetail(");
			strSql.Append("OrderNum,UserId,GoodsName,GoodsPrice,GoodsNum,RelationId,CreatDate)");
			strSql.Append(" values (");
			strSql.Append("@OrderNum,@UserId,@GoodsName,@GoodsPrice,@GoodsNum,@RelationId,@CreatDate)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderNum", SqlDbType.NVarChar,50),
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@GoodsName", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsPrice", SqlDbType.Decimal,9),
					new SqlParameter("@GoodsNum", SqlDbType.Int,4),
					new SqlParameter("@RelationId", SqlDbType.Int,4),
					new SqlParameter("@CreatDate", SqlDbType.DateTime)};
			parameters[0].Value = model.OrderNum;
			parameters[1].Value = model.UserId;
			parameters[2].Value = model.GoodsName;
			parameters[3].Value = model.GoodsPrice;
			parameters[4].Value = model.GoodsNum;
			parameters[5].Value = model.RelationId;
			parameters[6].Value = model.CreatDate;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Com.HaoJun.Mall.Model.GoodsOrderDetail model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update GoodsOrderDetail set ");
			strSql.Append("OrderNum=@OrderNum,");
			strSql.Append("UserId=@UserId,");
			strSql.Append("GoodsName=@GoodsName,");
			strSql.Append("GoodsPrice=@GoodsPrice,");
			strSql.Append("GoodsNum=@GoodsNum,");
			strSql.Append("RelationId=@RelationId,");
			strSql.Append("CreatDate=@CreatDate");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderNum", SqlDbType.NVarChar,50),
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@GoodsName", SqlDbType.NVarChar,50),
					new SqlParameter("@GoodsPrice", SqlDbType.Decimal,9),
					new SqlParameter("@GoodsNum", SqlDbType.Int,4),
					new SqlParameter("@RelationId", SqlDbType.Int,4),
					new SqlParameter("@CreatDate", SqlDbType.DateTime),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.OrderNum;
			parameters[1].Value = model.UserId;
			parameters[2].Value = model.GoodsName;
			parameters[3].Value = model.GoodsPrice;
			parameters[4].Value = model.GoodsNum;
			parameters[5].Value = model.RelationId;
			parameters[6].Value = model.CreatDate;
			parameters[7].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from GoodsOrderDetail ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from GoodsOrderDetail ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Com.HaoJun.Mall.Model.GoodsOrderDetail GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,OrderNum,UserId,GoodsName,GoodsPrice,GoodsNum,RelationId,CreatDate from GoodsOrderDetail ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			Com.HaoJun.Mall.Model.GoodsOrderDetail model=new Com.HaoJun.Mall.Model.GoodsOrderDetail();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Com.HaoJun.Mall.Model.GoodsOrderDetail DataRowToModel(DataRow row)
		{
			Com.HaoJun.Mall.Model.GoodsOrderDetail model=new Com.HaoJun.Mall.Model.GoodsOrderDetail();
			if (row != null)
			{
				if(row["Id"]!=null && row["Id"].ToString()!="")
				{
					model.Id=int.Parse(row["Id"].ToString());
				}
				if(row["OrderNum"]!=null)
				{
					model.OrderNum=row["OrderNum"].ToString();
				}
				if(row["UserId"]!=null && row["UserId"].ToString()!="")
				{
					model.UserId=int.Parse(row["UserId"].ToString());
				}
				if(row["GoodsName"]!=null)
				{
					model.GoodsName=row["GoodsName"].ToString();
				}
				if(row["GoodsPrice"]!=null && row["GoodsPrice"].ToString()!="")
				{
					model.GoodsPrice=decimal.Parse(row["GoodsPrice"].ToString());
				}
				if(row["GoodsNum"]!=null && row["GoodsNum"].ToString()!="")
				{
					model.GoodsNum=int.Parse(row["GoodsNum"].ToString());
				}
				if(row["RelationId"]!=null && row["RelationId"].ToString()!="")
				{
					model.RelationId=int.Parse(row["RelationId"].ToString());
				}
				if(row["CreatDate"]!=null && row["CreatDate"].ToString()!="")
				{
					model.CreatDate=DateTime.Parse(row["CreatDate"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,OrderNum,UserId,GoodsName,GoodsPrice,GoodsNum,RelationId,CreatDate ");
			strSql.Append(" FROM GoodsOrderDetail ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,OrderNum,UserId,GoodsName,GoodsPrice,GoodsNum,RelationId,CreatDate ");
			strSql.Append(" FROM GoodsOrderDetail ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM GoodsOrderDetail ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from GoodsOrderDetail T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "GoodsOrderDetail";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

