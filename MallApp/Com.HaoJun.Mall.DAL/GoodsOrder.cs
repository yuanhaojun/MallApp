﻿/**  版本信息模板在安装目录下，可自行修改。
* GoodsOrder.cs
*
* 功 能： N/A
* 类 名： GoodsOrder
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/26 22:30:28   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Com.HaoJun.Mall.DB;
namespace Com.HaoJun.Mall.DAL
{
	/// <summary>
	/// 数据访问类:GoodsOrder
	/// </summary>
	public partial class GoodsOrder
	{
		public GoodsOrder()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "GoodsOrder"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from GoodsOrder");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(Com.HaoJun.Mall.Model.GoodsOrder model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into GoodsOrder(");
			strSql.Append("OrderNum,UserId,TotalPrice,PayDate,State,PayPrice,AdressId,ValueIds,CreatDate)");
			strSql.Append(" values (");
			strSql.Append("@OrderNum,@UserId,@TotalPrice,@PayDate,@State,@PayPrice,@AdressId,@ValueIds,@CreatDate)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderNum", SqlDbType.NVarChar,50),
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@TotalPrice", SqlDbType.Decimal,9),
					new SqlParameter("@PayDate", SqlDbType.DateTime),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@PayPrice", SqlDbType.Decimal,9),
					new SqlParameter("@AdressId", SqlDbType.Int,4),
					new SqlParameter("@ValueIds", SqlDbType.NVarChar,50),
					new SqlParameter("@CreatDate", SqlDbType.DateTime)};
			parameters[0].Value = model.OrderNum;
			parameters[1].Value = model.UserId;
			parameters[2].Value = model.TotalPrice;
			parameters[3].Value = model.PayDate;
			parameters[4].Value = model.State;
			parameters[5].Value = model.PayPrice;
			parameters[6].Value = model.AdressId;
			parameters[7].Value = model.ValueIds;
			parameters[8].Value = model.CreatDate;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Com.HaoJun.Mall.Model.GoodsOrder model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update GoodsOrder set ");
			strSql.Append("OrderNum=@OrderNum,");
			strSql.Append("UserId=@UserId,");
			strSql.Append("TotalPrice=@TotalPrice,");
			strSql.Append("PayDate=@PayDate,");
			strSql.Append("State=@State,");
			strSql.Append("PayPrice=@PayPrice,");
			strSql.Append("AdressId=@AdressId,");
			strSql.Append("ValueIds=@ValueIds,");
			strSql.Append("CreatDate=@CreatDate");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@OrderNum", SqlDbType.NVarChar,50),
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@TotalPrice", SqlDbType.Decimal,9),
					new SqlParameter("@PayDate", SqlDbType.DateTime),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@PayPrice", SqlDbType.Decimal,9),
					new SqlParameter("@AdressId", SqlDbType.Int,4),
					new SqlParameter("@ValueIds", SqlDbType.NVarChar,50),
					new SqlParameter("@CreatDate", SqlDbType.DateTime),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.OrderNum;
			parameters[1].Value = model.UserId;
			parameters[2].Value = model.TotalPrice;
			parameters[3].Value = model.PayDate;
			parameters[4].Value = model.State;
			parameters[5].Value = model.PayPrice;
			parameters[6].Value = model.AdressId;
			parameters[7].Value = model.ValueIds;
			parameters[8].Value = model.CreatDate;
			parameters[9].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from GoodsOrder ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from GoodsOrder ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Com.HaoJun.Mall.Model.GoodsOrder GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,OrderNum,UserId,TotalPrice,PayDate,State,PayPrice,AdressId,ValueIds,CreatDate from GoodsOrder ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			Com.HaoJun.Mall.Model.GoodsOrder model=new Com.HaoJun.Mall.Model.GoodsOrder();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Com.HaoJun.Mall.Model.GoodsOrder DataRowToModel(DataRow row)
		{
			Com.HaoJun.Mall.Model.GoodsOrder model=new Com.HaoJun.Mall.Model.GoodsOrder();
			if (row != null)
			{
				if(row["Id"]!=null && row["Id"].ToString()!="")
				{
					model.Id=int.Parse(row["Id"].ToString());
				}
				if(row["OrderNum"]!=null)
				{
					model.OrderNum=row["OrderNum"].ToString();
				}
				if(row["UserId"]!=null && row["UserId"].ToString()!="")
				{
					model.UserId=int.Parse(row["UserId"].ToString());
				}
				if(row["TotalPrice"]!=null && row["TotalPrice"].ToString()!="")
				{
					model.TotalPrice=decimal.Parse(row["TotalPrice"].ToString());
				}
				if(row["PayDate"]!=null && row["PayDate"].ToString()!="")
				{
					model.PayDate=DateTime.Parse(row["PayDate"].ToString());
				}
				if(row["State"]!=null && row["State"].ToString()!="")
				{
					model.State=int.Parse(row["State"].ToString());
				}
				if(row["PayPrice"]!=null && row["PayPrice"].ToString()!="")
				{
					model.PayPrice=decimal.Parse(row["PayPrice"].ToString());
				}
				if(row["AdressId"]!=null && row["AdressId"].ToString()!="")
				{
					model.AdressId=int.Parse(row["AdressId"].ToString());
				}
				if(row["ValueIds"]!=null)
				{
					model.ValueIds=row["ValueIds"].ToString();
				}
				if(row["CreatDate"]!=null && row["CreatDate"].ToString()!="")
				{
					model.CreatDate=DateTime.Parse(row["CreatDate"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,OrderNum,UserId,TotalPrice,PayDate,State,PayPrice,AdressId,ValueIds,CreatDate ");
			strSql.Append(" FROM GoodsOrder ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,OrderNum,UserId,TotalPrice,PayDate,State,PayPrice,AdressId,ValueIds,CreatDate ");
			strSql.Append(" FROM GoodsOrder ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM GoodsOrder ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from GoodsOrder T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "GoodsOrder";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

