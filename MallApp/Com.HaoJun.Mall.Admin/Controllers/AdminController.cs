﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Com.HaoJun.Mall.Admin.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/ 
        Com.HaoJun.Mall.BLL.AdminUser bll = new BLL.AdminUser();
        /// <summary>
        /// 此页面时管理员的详情页
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(int id = 0)
        {
            ViewBag.Id = id;
            ViewBag.tt = "添加管理员";
                if (id > 0)
                {
                ViewBag.tt = "修改密码";
                }
            return View();
            }
        //[HttpPost]
        //public ActionResult IndexPost(Com.HaoJun.Mall.Model.AdminUser model)
        //{
        //    if (model.Id > 0)
        //    {
        //        bll.Update(model);
        //    }
        //    else
        //    {
        //        bll.Add(model);
        //    }
        //    return RedirectToAction("List","Admin");
        //}
        public ActionResult List()
            {
            //if (Request.Cookies["IsAdmin"].Value!="1")
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            var list = bll.GetModelList("");
            return View(list);
        }
    }
}