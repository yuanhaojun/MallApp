﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Com.HaoJun.Mall.Admin.Startup))]
namespace Com.HaoJun.Mall.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
